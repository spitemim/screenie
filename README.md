<h1 align=center>screenie - a simple screenshot utility</h1>
<div align=center><img src='menu.png' alt='screenshot of the screenie menu interface' /></div>

screenie is a simple screenshot utility. It takes screenshots with scrot, and
opens a menu for what to do with the screenshot, like uploading to 0x0, or
copying to the clipboard.

## Installation

This script uses `scrot`, `xclip`, `xdg-user-dir`, `dmenu`, and `notify-send`. Make sure
all these commands are installed on your system.

Then, to install, run:

<pre>
git clone https://git.spitemim.xyz/screenie
cd screenie
sudo cp screenie /usr/bin
</pre>

## Tips

* Edit the line beginning with `screenshotdir=` if you want to specify a custom
default location.

## License

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program. If not, see <https://www.gnu.org/licenses/>.
