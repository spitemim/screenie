#!/bin/sh
# deps: scrot, xclip, xdg-user-dir, dmenu, notify-send

tmpfile="/tmp/screenie.png"
pixdir="$(xdg-user-dir PICTURES)"

defaultname="$(date +%Y-%m-%d_%H-%M-%S.png)"
screenshotdir="${pixdir:-/home}/screenies"

outputdir="$screenshotdir"
filename="$defaultname"

warning() {
	notify-send -i dialog-error screenie "$*"
}

error() {
	warning "$*"
	exit 1
}

pickdir() {
	outputdir="$( (printf ".\n"; find "$PWD" -maxdepth 4 -name ".*" -prune -o -type d -print) |
		dmenu -p "pick directory>" -l 20)" || error "Aborted"

	if [ ! -d "$outputdir" ]
	then
		warning "Specified dir is not a directory"
		pickdir
	fi
}

pickname() {
	filename="$(printf "%s" "$defaultname" |
		dmenu -p "name file>" -l 1)"  || error "Aborted"

	if [ -d "$filename" ]
	then
		warning "Specified filename is a directory"
		pickname
		return
	fi

	if [ -f "$filename" ]
	then
		[ "$(printf "no\nyes" | dmenu -p "$filename already exists. replace?")" = "yes" ] || error "Not saving"
	fi

	if [ "$(printf "%s" "$filename" | sed -E "s/^.*\.([^.]+)/\1/")" != "png" ]
	then
		if [ "$(printf "yes\nno" | dmenu -p "filename doesn't end in .png -- fix extension?")" = "yes" ]
		then
			filename="$(printf "%s" "$filename" | sed -E "s/^([^.]+).*$/\1.png/")"
		fi
	fi
}

copy() {
	if xclip -sel c -t image/png < /tmp/screenie.png
	then
		notify-send -i "$tmpfile" screenie "Copied to clipboard"
	else
		error "Could not copy to clipboard (error $?)"
	fi
}


save() {
	if cp /tmp/screenie.png "$outputdir"/"$filename"
	then
		notify-send -i "$tmpfile" screenie "Screenshot saved to $outputdir/$filename"
	else
		error "Could not save file (error $?)"
	fi
}

upload() {
	notify-send -i dialog-information screenie "Starting upload..."

	if url="$(curl -F"file=@$tmpfile" https://0x0.st)"
	then
		url="$(printf "%s" "$url" | sed 's/http:/https:/')"
		printf "%s" "$url" | xclip -sel c
		notify-send -i "$tmpfile" screenie "$url (Copied to clipboard)"
	else
		error "Upload failed (error $?)"
	fi
}

mkdir -p "$screenshotdir"

if ! scrot -zfs - > "$tmpfile"
then
	error "Screenshot aborted"
	exit 1
fi

choice="$(printf "1 - Copy & save to %s\n2 - Copy\n3 - Save\n4 - Save to custom location\n5 - Upload to 0x0.st\n7 - Exit\n" "$screenshotdir" |
	dmenu -p "screenie>" -l 6)"

case "$choice" in
	1*)
		copy
		save
		;;
	2*)
		copy
		;;
	3*)
		pickname
		save
		;;
	4*)
		pickdir
		pickname
		save
		;;
	5*)
		upload
		;;
esac

rm -f "$tmpfile"
